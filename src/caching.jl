
"""
    Fetcher

Abstract type for retrieving byte arrays from arbitrary sources.  Used by [`CacheVector`](@ref) for
retrieving data.  Each `Fetcher` represents a single object (i.e. addressable in a contiguous range) of a
file-system or file-system-like interface.

## Fetcher Interface

### Required Methods
- `Parquet2.fetch(f, ab::AbstractUnitRange)`: Retrieve a `Vector{UInt8}` identical to bytes `a:b`
    (1-based index, inclusive) of the underlying object.
- `Base.length(f)`: The total number of bytes of the object the fetcher fetches data from.

### Optional Methods
- `Fetcher(p::AbstractPath; kw...)`: Defines an appropriate fetcher for the path type.
- `PageBuffer(f, a::Integer, b::Integer)`: An object for accessing views of the underlying data.  The provided
    buffer *must* be a `Vector{UInt8}`.  The definition defaults to `PageBuffer(fetch(f, a:b), a, b)`.
    See [`PageBuffer`](@ref).
"""
abstract type Fetcher end

Base.getindex(f::Fetcher, ab::AbstractUnitRange) = fetch(f, ab)
Base.firstindex(f::Fetcher) = 1
Base.lastindex(f::Fetcher) = length(f)

Base.close(::Fetcher) = nothing


"""
    PageBuffer

Represents a view into a byte buffer that guarantees the underlying data is a `Vector{UInt8}`.
"""
struct PageBuffer
    v::Vector{UInt8}
    a::Int
    b::Int
end

Base.view(pb::PageBuffer, δ::Integer=0) = view(pb.v, (pb.a + δ):pb.b)

Base.firstindex(pb::PageBuffer) = pb.a
Base.lastindex(pb::PageBuffer) = pb.b

Base.length(pb::PageBuffer) = pb.b - pb.a + 1

PageBuffer(f::Fetcher, a::Integer, b::Integer) = PageBuffer(fetch(f, a:b), a, b)

"""
    GenericFetcher

Wraps a function which fetches byte arrays to provide a `Fetcher` interface.
"""
struct GenericFetcher{ℱ} <: Fetcher
    ℓ::Int
    𝒻::ℱ
end

GenericFetcher(𝒻, ℓ::Integer; kw...) = GenericFetcher{typeof(𝒻)}(ℓ, 𝒻)

Base.length(f::GenericFetcher) = f.ℓ

fetch(f::GenericFetcher, ab::AbstractUnitRange) = f.𝒻(ab)


"""
    IOFetcher <: Fetcher

Provides a `Fetcher` interface for an `IO` stream object.
"""
struct IOFetcher{ℐ<:IO} <: Fetcher
    io::ℐ
    ℓ::Integer
end

Base.length(f::IOFetcher) = f.ℓ

Base.close(f::IOFetcher) = close(f.io)

function fetch(f::IOFetcher, ab::AbstractUnitRange)
    seek(f.io, a-1)
    read(f.io, length(ab))
end

IOFetcher(p::AbstractPath; kw...) = IOFetcher(open(p), diskusage(p))


"""
    VectorFetcher <: Fetcher

Provides a `Fetcher` interface for an in-memory (or memory-mapped) array.
"""
struct VectorFetcher <: Fetcher
    v::Vector{UInt8}

    VectorFetcher(v::AbstractVector{UInt8}; kw...) = new(v)
end

Base.length(f::VectorFetcher) = length(f.v)

"""
    fetch(f::Fetcher, ab)

Fetch the byte range `ab`.  This always copies.  To avoid copying, use `PageBuffer` which is non-copying
when possible.
"""
fetch(f::VectorFetcher, ab::AbstractUnitRange) = f.v[ab]

PageBuffer(f::VectorFetcher, a::Integer, b::Integer) = PageBuffer(f.v, a, b)

is_mmapable_path(p::AbstractPath) = false
is_mmapable_path(p::SystemPath) = true

function Fetcher(p::AbstractPath; kw...)
    # fall back to vector rather than IO because it is more likely to have an implementation for
    # unknown path types
    VectorFetcher(read(p); kw...)
end

Fetcher(p::SystemPath; use_mmap::Bool=true, kw...)  = use_mmap ? Fetcher(Mmap.mmap(p); kw...) : IOFetcher(p; kw...)

Fetcher(p::AbstractString; kw...) = Fetcher(AbstractPath(p); kw...)

Fetcher(v::AbstractVector{UInt8}; kw...) = VectorFetcher(v)


#TODO: locks!!!
"""
    CacheVector <: AbstractVector{UInt8}

An array that automatically caches blocks of data.  The envisioned use case is selectively reading subsets of a large
remote file.  Contains a [`Fetcher`](@ref) object for retrieving data to be cached.

Note that the performance of indexing `CacheVector` is quite poor for single indices.  It is intended that
`CacheVector` is indexed by the largest subsets possible.

## Constructors
```julia
CacheVector(f::Fetcher; subset_length=default_subset_length(f), max_subsets=typemax(Int), preload=false)
```

## Arguments
- `f`: [`Fetcher`](@ref) object for retrieving data.
- `subset_length::Integer`: the length of each subset. This controls the size of blocks retrieved in a single call.
- `max_subsets::Integer`: the number of subsets that can be stored before they start to be evicted.
"""
struct CacheVector{ℱ} <: AbstractArray{UInt8,1}
    cache::LRU{HashableUnitRange{Int},Vector{UInt8}}
    subset_length::Int
    n_subsets::Int
    fetcher::ℱ
end

Base.size(v::CacheVector) = (length(v.fetcher),)

Base.IndexStyle(::Type{<:CacheVector}) = IndexLinear()

function _make_index_range(v::CacheVector, a::Integer)
    HashableUnitRange(((a-1)*v.subset_length + 1):min(length(v), a*v.subset_length))
end

function Base.get!(v::CacheVector, r::AbstractUnitRange{<:Integer})
    r = HashableUnitRange{Int}(r)
    get!(() -> fetch(v.fetcher, r), v.cache, r)
end

Base.@propagate_inbounds function subsetindex(v::CacheVector, i::Integer)
    @boundscheck checkbounds(v, i)
    a, b = fldmod1(i, v.subset_length)
    _make_index_range(v, a), b
end

function Base.getindex(v::CacheVector, i::Int)
    sidx, r = subsetindex(v, i)
    w = get!(v, sidx)
    @inbounds w[r]
end

function Base.getindex(v::CacheVector, ab::AbstractUnitRange{<:Integer})
    ab = HashableUnitRange{Int}(ab)
    @boundscheck checkbounds(v, ab)
    a1, b1 = fldmod1(first(ab), v.subset_length)
    a2, b2 = fldmod1(last(ab), v.subset_length)
    if a1 == a2
        sidx = _make_index_range(v, a1)
        w = get!(v, sidx)
        @inbounds w[b1:b2]
    else
        # now we have to cleverly concatenate sections of arrays without too much extra allocation
        idxs = a1:a2 |> Map(α -> _make_index_range(v, α))
        idxs |> Map(idx -> get!(v, idx)) |> Enumerate() |> Map() do (i, w)
            if i == 1
                w[b1:end]
            elseif i == (a2 - a1 + 1)
                w[1:b2]
            else
                w
            end
        end |> Cat() |> collect
    end
end

fetchertype(v::CacheVector{ℱ}) where {ℱ} = ℱ

Base.getindex(v::CacheVector, ::Colon) = getindex(v, firstindex(v):lastindex(v))

"""
    default_subset_length(f::Fetcher)

Sets the default length of subsets pulled by a [`Fetcher`](@ref) object.  This should be defined when
implementing new `Fetcher`s.
"""
default_subset_length(f::Fetcher) = 100*1024^2  # ∼100 MB
default_subset_length(f::VectorFetcher) = max(1, length(f))

ncached(v::CacheVector) = length(v.cache)

function _CacheVector_nopreload(f::Fetcher;
                                subset_length::Integer=default_subset_length(f),
                                max_subsets::Integer=typemax(Int))
    nsbs = ÷(length(f), subset_length, RoundUp)
    CacheVector(LRU{HashableUnitRange{Int},Vector{UInt8}}(;maxsize=max_subsets),
                subset_length, nsbs, f)
end

function CacheVector(f::Fetcher;
                     subset_length::Integer=default_subset_length(f),
                     max_subsets::Integer=typemax(Int),
                     preload::Bool=false,
                    )
    _CacheVector_nopreload(f; subset_length, max_subsets)
end
function CacheVector(f::VectorFetcher;
                     subset_length::Integer=default_subset_length(f),
                     max_subsets::Integer=typemax(Int),
                     preload::Bool=true,
                    )
    if preload
        c = LRU{HashableUnitRange{Int},Vector{UInt8}}(;maxsize=max_subsets)
        c[HashableUnitRange(1:length(f.v))] = f.v
        CacheVector(c, length(f.v), 1, f)
    else
        _CacheVector_nopreload(f; subset_length, max_subsets)
    end
end
CacheVector(v::AbstractVector{UInt8}; kw...) = CacheVector(VectorFetcher(v); kw...)

function CacheVector(f::Fetcher, opts::ReadOptions)
    subset_length = isnothing(opts.subset_length) ? default_subset_length(f) : opts.subset_length
    CacheVector(f;
                subset_length, max_subsets=opts.max_subsets, preload=opts.preload,
               )
end
CacheVector(v::AbstractVector{UInt8}, opts::ReadOptions) = CacheVector(VectorFetcher(v), opts)

function PageBuffer(v::CacheVector, a::Integer, b::Integer)
    sidx1, _ = subsetindex(v, a)
    sidx2, _ = subsetindex(v, b)
    if sidx1 == sidx2
        PageBuffer(get!(v, sidx1), a - first(sidx1) + 1, b - first(sidx1) + 1)
    else
        w = v[a:b]
        PageBuffer(w, 1, length(w))
    end
end


"""
    CacheIO

Wraps a `CacheVector` object in an `IO` interface.  By default, the `IO` will read from the `CacheVector` normally
and caching occurs as it would if indexing the `CacheVector` directly.  If a range is provided, *only* data
from that range will be used, and reading past it will result in an error.
"""
mutable struct CacheIO{ℱ<:Fetcher} <: IO
    v::CacheVector{ℱ}
    range::HashableUnitRange{Int}
    pos::Int

    CacheIO(v::CacheVector, range::AbstractUnitRange=1:length(v), pos::Integer=0) = new{fetchertype(v)}(v, range, pos)
end

CacheIO(v::CacheVector, pos::Integer) = CacheIO(v, 1:length(v), pos)

Base.eof(io::CacheIO) = io.pos ≥ last(io.range)

function Base.read(io::CacheIO, ::Type{UInt8})
    if first(io.range) == 1
        @inbounds io.v[io.pos += 1]
    else
        w = get!(io.v, io.range)
        io.pos += 1
        @inbounds w[io.pos - first(io.range) + 1]
    end
end

Base.seek(io::CacheIO, pos::Integer) = (io.pos = pos; nothing)
Base.seekstart(io::CacheIO) = seek(io, 0)

Base.position(io::CacheIO) = io.pos

function Base.unsafe_read(io::CacheIO, p::Ptr{UInt8}, n::UInt)
    pb = if first(io.range) == 1  # we don't need a special get!
        PageBuffer(io.v, io.pos+1, io.pos+n)
    else  # we are asking to use a special get!
        w = get!(io.v, io.range)
        PageBuffer(w, io.pos + 1 - first(io.range) + 1, length(w))
    end
    io.pos += n
    unsafe_copyto!(p, pointer(pb.v, pb.a), n)
end

