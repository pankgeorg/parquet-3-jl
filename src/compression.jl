
"""
    getcodecname(n::Integer)

Get the name (`Symbol`) of the codec corresponding to the integer `n` in the parquet metadata schema.
"""
getcodecname(n::Integer) = thriftenum(Meta.CompressionCodec, n)

"""
    getcompressor(s::Symobl)

Get the function `𝒻(::AbstractVector{UInt8})::AbstractVector{UInt8}` for compressing data to codec `s`.
"""
getcompressor(s::Symbol) = getcompressor(Val(s))
getcompressor(n::Integer) = getcompressor(getcodecname(n))

"""
    getdecompressor(s::Symbol)

Get the function `𝒻(::AbstractVector{UInt8})::AbstractVector{UInt8}` for decompressing data from codec `s`.
"""
getdecompressor(s::Symbol) = getdecompressor(Val(s))
getdecompressor(n::Integer) = getdecompressor(getcodecname(n))


getcompressor(::Val) = throw(ArgumentError("unknown compression codec $s"))
getdecompressor(::Val) = throw(ArgumentError("unknown compression codec $s"))

getcompressor(::Val{:uncompressed}) = identity
getdecompressor(::Val{:uncompressed}) = identity

getcompressor(::Val{:snappy}) = Snappy.compress ∘ Vector
getdecompressor(::Val{:snappy}) = Snappy.uncompress ∘ Vector

getcompressor(::Val{:gzip}) = v -> transcode(GzipCompressor, Vector(v))
getdecompressor(::Val{:gzip}) = v -> transcode(GzipDecompressor, Vector(v))

getcompressor(::Val{:zstd}) = v -> transcode(ZstdCompressor, Vector(v))
getdecompressor(::Val{:zstd}) = v -> transcode(ZstdDecompressor, Vector(v))

function compress(V::Val, v)
    io = IOBuffer()
    writepadded(io, v)
    getcompressor(V)(take!(io))
end

compress(s::Symbol, v) = compress(Val(s), v)

function thrift_codec_code(s::Symbol)
    Int32(if s == :uncompressed
        0
    elseif s == :snappy
        1
    elseif s == :gzip
        2
    elseif s == :lzo
        3
    elseif s == :brotli
        4
    elseif s == :lz4
        5
    elseif s == :zstd
        6
    elseif s == :lz4_raw
        7
    else
        throw(ArgumentError("invalid compression codec $s"))
    end)
end
thrift_codec_code(::Val{𝒮}) where {𝒮} = thrift_codec_code(𝒮)
