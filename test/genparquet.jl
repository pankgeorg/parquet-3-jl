using PyCall, Dates, UUIDs, DataFrames, Random, BenchmarkTools, OrderedCollections, JSON3, LightBSON, Transducers
using Parquet2

include("gentables.jl")

const pynone = pybuiltin("None")

const fastparquet = pyimport("fastparquet")
const pandas = pyimport("pandas")
const pyarrow = pyimport("pyarrow")
const pyarrowq = pyimport("pyarrow.parquet")

# these are Python stdlibs so they should always be present
const pyuuid = pyimport("uuid")
const pydecimal = pyimport("decimal")
const pydatetime = pyimport("datetime")


function pandas_compat_col(k, v)
    if occursin("dictionar", string(k))
        # only supporting these for ints right now
        any(ismissing, v) && (v = replace(v, missing=>nothing))
        pandas.Series(v, dtype="category")
    elseif nonmissingtype(eltype(v)) <: Integer
        pandas.Series(replace(v, missing=>nothing), dtype="Int64")
    elseif any(ismissing, v)
        pandas.Series(replace(v, missing=>nothing))
    else
        v
    end
end

function pandas_compat_dict(tbl)
    pairs(tbl) |> MapSplat((k,v) -> string(k)=>pandas_compat_col(k,v)) |> OrderedDict
end

to_pandas(tbl) = pandas.DataFrame(pandas_compat_dict(tbl))


function save_standard_test_fastparquet(df=to_pandas(standard_test_table()))
    colnames = filter(s -> occursin("missing", s), collect(df))
    fastparquet.write(testfilename(:std_fastparquet), df; has_nulls=colnames)
end

function save_random_test_fastparquet(df=to_pandas(random_test_table()))
    colnames = collect(df)
    fastparquet.write(testfilename(:rand_fastparquet), df;
                      has_nulls=filter(s -> occursin("_missing", s), colnames),
                     )
end

function save_standard_test_pyarrow(df=to_pandas(standard_test_table()))
    pyarrowq.write_table(pyarrow.Table.from_pandas(df), testfilename(:std_pyarrow), row_group_size=df.shape[1]÷2,
                         version="2.6")
end

function save_random_test_pyarrow(df=to_pandas(random_test_table()))
    pyarrowq.write_table(pyarrow.Table.from_pandas(df), testfilename(:rand_pyarrow), row_group_size=df.shape[1]÷2,
                         version="2.6")
end

function save_extra_types_fastparquet()
    df = pandas.DataFrame()
    set!(df, "jsons", make_json_dicts(5))
    #set!(df, "bsons", make_json_dicts(5))  # fastparquet seems to have dropped BSON
    set!(df, "jvm_timestamps", [DateTime(2022,3,8) + Day(i) + Minute(1) for i ∈ 0:4])
    set!(df, "fixed_strings", ["a", "ab", "abc", "abcd", "abcde"])
    set!(df, "bools", [false, true, true, false, true])
    fastparquet.write(testfilename(:extra_types_fastparquet), df,
                      times="int96",
                      # it doesn't actually make these strings for some reason, but they are still
                      # fixed arrays
                      fixed_text=Dict("fixed_strings"=>5),
                      object_encoding=Dict("jsons"=>"json", "bsons"=>"bson", "bools"=>"bool")
                     )
end

function save_extra_types_pyarrow()
    df = pandas.DataFrame()
    set!(df, "decimals", pydecimal.Decimal.(["1.0", "2.1", "3.2", "4.3", "5.4"]))
    set!(df, "dates", [Date(1990,1,i) for i ∈ 1:5])
    set!(df, "times_of_day", [pydatetime.time(i) for i ∈ 1:5])
    pyarrowq.write_table(pyarrow.Table.from_pandas(df), testfilename(:extra_types_pyarrow), version="2.6")
end

function save_hive_fastparquet()
    # if you don't delete it might get bizarre mixed thing
    rm(testfilename(:hive_fastparquet), recursive=true, force=true)
    df = pandas.DataFrame()
    set!(df, "A", [1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3])
    set!(df, "B", [fill("alpha", 8); fill("beta", 4)])
    set!(df, "data1", 1.0:12.0)
    set!(df, "data2", [1:11; nothing])
    fastparquet.write(testfilename(:hive_fastparquet), df,
                      file_scheme="hive",
                      partition_on=["A", "B"],
                     )
end

function _compression_arg(tbl)
    cs = ["SNAPPY", "GZIP", "ZSTD"]
    o = Dict{String,Any}()
    for (i, (k,_)) ∈ enumerate(pairs(tbl))
        c = cs[mod1(i, length(cs))]
        o[string(k)] = c
    end
    o
end

function save_compressed_fastparquet(tbl=standard_test_table())
    df = to_pandas(tbl)
    fastparquet.write(testfilename(:compressed_fastparquet), df,
                      compression=_compression_arg(tbl),
                     )
end

function save_compressed_pyarrow(tbl=random_test_table())
    df = to_pandas(tbl)
    pyarrowq.write_table(pyarrow.Table.from_pandas(df), testfilename(:compressed_pyarrow), version="2.6",
                         compression=_compression_arg(tbl),
                        )
end

# note that we are not currently generating the table "simple_spark" since we copy it from fastparquet
function save_all()
    isdir("data") || mkdir("data")
    save_standard_test_fastparquet()
    save_standard_test_pyarrow()
    save_random_test_fastparquet()
    save_random_test_pyarrow()
    save_extra_types_fastparquet()
    save_extra_types_pyarrow()
    save_hive_fastparquet()
    save_compressed_fastparquet()
    save_compressed_pyarrow()
end

pyload(file::AbstractString; kw...) = fastparquet.ParquetFile(file; kw...).to_pandas()
pyload(file::Symbol; kw...) = pyload(testfilename(file); kw...)

testload(file::Symbol; kw...) = Parquet2.Dataset(testfilename(file); kw...)

function pyloadbuffer_fastparquet(v::AbstractVector{UInt8}; kw...)
    mktemp() do path, io
        write(io, v)
        close(io)
        fastparquet.ParquetFile(path; kw...).to_pandas()
    end
end

function pyloadbuffer_pyarrow(v::AbstractVector{UInt8}; kw...)
    mktemp() do path, io
        write(io, v)
        close(io)
        pyarrowq.read_table(path; kw...).to_pandas()
    end
end
