```@meta
CurrentModule = Parquet2
```

# Internals


## `FilePathsBase` Integration
This package uses [FilePathsBase.jl](https://github.com/rofinn/FilePathsBase.jl) to infer the type
of path to an object to read and the appropriate way to read it.

An `AbstractPath` object is passed to a `Fetcher` object which provides a simple, minimal
interface for fetching contiguous blocks of data from the source.

The `FileManager` object is used to track which files are referred to by the parquet metadata.

```@docs
FileManager
```

### Implementing New Fetchers
The package can be easily extended by implementing new `Fetcher` objects.  The provide, primarily,
two methods, `Base.length` and `Parquet2.fetch`.  The former simply gives the total size of the
source object in bytes, while the latter provides a method for reading contiguous blocks of data
from it.

```@docs
Fetcher
```


## Data Caching
This package does everything as lazily as possible by default.  While files on the local file system
are memory mapped by default, if memory mapping is disabled or if reading from a remote file system,
only the top-level metadata is read in fully.  Everything else is read in by blocks.  By default
these are approximately ``100~\textrm{MB}`` each (exactly `100*1024^2` bytes).  The `max_subsets`
argment can be provided to the `Dataset` to dictate the maximum number of these blocks that can be
loaded at any given time (by default this is effectively unlimited).

The caching is achieved by a `CacheVector`, which provides an interface for the data buffers from
which all data is read.  The `CacheVector` can be accessed like a normal `AbstractVector` but
internally caches only subsets of this at a time.

The `CacheVector` uses `Fetcher` objects to fetch the blocks, and in this way provides an
interface for loading data by blocks from any source that supports this.  Sources that do not can be
loaded in as a single vector.

!!! warning
    
    `CacheVector` has extremely inefficient indexing for individual indices, but scales favorably
    when indexed by large contiguous blocks.  When loading parquet files `CacheVector` are only used
    for loading entire pages at a time (usually smaller than the blocks).  Note that `CacheVector`
    indexing is also much less efficient when hitting bloc boundaries, so some alignment improves
    the performance of this package.

```@docs
CacheVector
```

## [API](@id internals_api)
```@docs
IOFetcher
GenericFetcher
VectorFetcher
ParquetType
PageBuffer
PageIterator
PageLoader
BitUnpackVector
PooledVector
ParqRefVector

decompressedpageview

bitpack
bitpack!
bitmask
bitjustify
bitwidth
bytewidth
readfixed
writefixed
HybridIterator
encodehybrid_bitpacked
encodehybrid_rle

maxdeflevel
maxreplevel
leb128encode
leb128decode

OptionSet
ReadOptions
WriteOptions
```
